<?php

namespace PayDay\Services;

use App\User\Entity\ServiceLog;
use App\User\Entity\UserUaCreditplus;
use Core\App;
use ErrorException;
use PayDay\Services\CollectionHttpClient\CollectionHttpClientInterface;
use PayDay\Services\CollectionHttpClient\HubInnCollectionHttpClient;
use PayDay\Services\CollectionHttpClient\HubPhoneCollectionHttpClient;

class SearchByHubService
{
    public const SEARCH_BY_PHONE = 'phone';
    public const SEARCH_BY_INN = 'inn';

    /** @var CollectionHttpClientInterface $restService */
    private $restService;

    private $searchType;

    private $providerMapping = [
        self::SEARCH_BY_PHONE => HubPhoneCollectionHttpClient::class,
        self::SEARCH_BY_INN => HubInnCollectionHttpClient::class,
    ];

    /**
     * SearchByHubService constructor.
     * @param string $type
     * @throws ErrorException
     */
    public function __construct(string $type)
    {
        if (!in_array($type, array_keys($this->providerMapping))) {
            throw new ErrorException(trans('Undefined search type'));
        }

        $this->searchType = $type;
        $this->restService = new $this->providerMapping[$type];
    }

    /**
     * @param string $searchField
     * @return array
     */
    public function getData(string $searchField): array
    {
        $this->restService->authorization();
        $authRequestData = $this->restService->getAuthorizationData();
        $this->saveRequest($authRequestData);
        $data = $this->restService->search($searchField);
        $this->saveRequest($this->restService->getRequestData(), $searchField);
        $this->restService->unsetRequestData();
        return $data;
    }

    /**
     * @param int $userId
     * @return string
     * @throws ErrorException
     */
    public function getLastSavedData(int $userId): string
    {
        $lastRecord = App::getRepository(ServiceLog::class)
            ->findOneBy(
                [
                    'userId' => $userId,
                    'serviceId' => $this->restService->getServiceId(),
                    'requestType' => 'GET',
                    'isSuccess' => 1
                ],
                [
                    'id' => 'DESC'
                ]
            );

        if ($lastRecord !== null) {
            return $lastRecord->getResponse();
        }

        throw new ErrorException(sprintf('Data by id `%s` not found', $userId));
    }

    /**
     * @param string $searchField
     * @return int|null
     */
    private function getUserId(string $searchField): ?int
    {
        if ($this->searchType === self::SEARCH_BY_PHONE) {
            $userUaCreditPlus = App::getRepository(UserUaCreditplus::class)->getUserByPhone($searchField);
        } else {
            $userUaCreditPlus = App::getRepository(UserUaCreditplus::class)
                ->findOneBy([$this->searchType => $searchField]);
        }
        return $userUaCreditPlus && $userUaCreditPlus->getUser() ? $userUaCreditPlus->getUser()->getId() : null;
    }

    /**
     * @param array $request
     * @param string $searchField
     */
    private function saveRequest(array $request, string $searchField = ''): void
    {
        $entity = new ServiceLog();
        $entity
            ->setServiceId($request['serviceId'])
            ->setUserId(!empty($searchField) ? $this->getUserId($searchField) : null)
            ->setAdminId($request['adminId'])
            ->setUrl($request['url'])
            ->setHeaders($request['headers'])
            ->setParams($request['params'])
            ->setResponse($request['response'])
            ->setRequestType(mb_strtoupper($request['requestType']))
            ->setResponseCode($request['responseCode'])
            ->setIsSuccess($request['isSuccess'])
            ->setIsCached($request['isCached'])
            ->setDate($request['date']);

        App::getRepository(ServiceLog::class)
            ->save($entity);
    }
}
