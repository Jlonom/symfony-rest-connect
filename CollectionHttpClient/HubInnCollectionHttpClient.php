<?php

namespace PayDay\Services\CollectionHttpClient;

use App\User\Entity\ServiceSettings;
use App\User\Entity\UserUaCreditplus;
use Core\App;

class HubInnCollectionHttpClient extends HubCollectionHttpClient
{
    /**
     * @param string $searchValue
     * @return array
     * @throws \ErrorException
     */
    public function search(string $searchValue): array
    {
        $this->requestHeaders = [
            'Authorization' => sprintf('Bearer %s', $this->jwt->access_token)
        ];
        $this->requestQuery = [
            $this->settings['ua_settings']['ua_credit_info']['bki_hub_search_inn_query'] => $searchValue
        ];
        $this->makeRequest($this->settings['ua_settings']['ua_credit_info']['bki_hub_search_inn_uri']);
        $userInfo = json_decode($this->responseContent, true);

        if ($this->checkIssetOkpo($userInfo)) {
            return $this->convertData($userInfo);
        }

        throw new \ErrorException('User is not found');
    }

    /**
     * @param array $userInfo
     * @return bool
     */
    private function checkIssetOkpo(array $userInfo): bool
    {
        return isset($userInfo['okpo']) && $userInfo['okpo'] !== null;
    }
}
