<?php

namespace PayDay\Services\CollectionHttpClient;

use Core\App;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;

abstract class BaseCollectionHttpClient implements CollectionHttpClientInterface
{
    protected $mainUrl;
    protected $authorizationUrl;

    protected $serviceId;
    protected $userId;

    /** @var Client $client */
    protected $client;
    protected $settings;
    protected $jwt;

    /** @var int $responseCode */
    protected $responseCode;
    /** @var string $responseContent */
    protected $responseContent;

    protected $requestHeaders = [];
    protected $requestQuery = [];
    protected $requestMethod = 'get';
    protected $requestParams = [];
    protected $requestAuth = [];

    /** @var string $requestedUrl */
    protected $requestUrl;

    /**
     * @param array $params
     * @throws \ErrorException
     */
    public function __construct(array $params = [])
    {
        $this->client = new Client([
            'base_uri' => $this->mainUrl,
            'allow_redirects' => false,
        ]);

        $this->init($params);
    }

    /**
     * @param array $params
     * @throws \ErrorException
     */
    public function init(array $params): void
    {
        foreach ($params as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \ErrorException(sprintf('Property `%s` does not exist in class `%s`', $key, self::class));
            }
            $this->{$key} = $value;
        }
    }

    abstract public function prepareAuthorizationData(): void;

    abstract public function afterAuthorizationProcess(): void;

    /**
     * @return string
     */
    abstract public function getServiceName(): string;

    public function authorization(): void
    {
        $this->prepareAuthorizationData();
        $this->makeRequest($this->authorizationUrl);
        $this->jwt = json_decode($this->responseContent);
        $this->afterAuthorizationProcess();
    }

    /**
     * @param string $uri
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function makeRequest(string $uri): void
    {
        $url = null;
        $response = $this->client->request(
            $this->requestMethod,
            $uri,
            [
                'headers' => !empty($this->requestHeaders) ? $this->requestHeaders : null,
                'query' => !empty($this->requestQuery) ? $this->requestQuery : null,
                'form_params' => !empty($this->requestParams) ? $this->requestParams : null,
                'auth' => $this->requestAuth,
                'on_stats' => function (TransferStats $stats) use (&$url) {
                    $url = $stats->getEffectiveUri();
                }
            ]
        );

        $this->responseCode = $response->getStatusCode();
        $this->responseContent = $response
            ->getBody()
            ->getContents();
        $this->requestUrl = $url;
    }

    /**
     * @return array
     */
    public function getRequestData(): array
    {
        return [
            'serviceId' => $this->serviceId,
            'userId' => $this->userId,
            'adminId' => 0,
            'url' => $this->requestUrl,
            'headers' => !empty($this->requestHeaders) ? json_encode($this->requestHeaders) : null,
            'params' => !empty($this->requestParams) ? json_encode($this->requestParams) : null,
            'response' => $this->responseContent,
            'requestType' => $this->requestMethod,
            'responseCode' => $this->responseCode,
            'isSuccess' => $this->responseCode === 200 ? 1 : 0,
            'isCached' => 0,
            'date' => new \DateTime()
        ];
    }

    public function unsetRequestData(): void
    {
        $this->requestHeaders = [];
        $this->requestQuery = [];
        $this->requestMethod = 'get';
        $this->requestParams = [];
        $this->requestAuth = [];
        $this->requestUrl = null;
        $this->responseCode = null;
        $this->responseContent = null;
    }
}
