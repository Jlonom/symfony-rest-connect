<?php

namespace PayDay\Services\CollectionHttpClient;

interface CollectionHttpClientInterface
{
    public function authorization(): void;

    /**
     * @param string $searchValue
     * @return array
     */
    public function search(string $searchValue): array;

    /**
     * @param string $uri
     */
    public function makeRequest(string $uri): void;
}
