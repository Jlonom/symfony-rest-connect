<?php

namespace PayDay\Services\CollectionHttpClient;

use App\User\Entity\ServiceSettings;
use App\User\Entity\UserUaCreditplus;
use Core\App;

abstract class HubCollectionHttpClient extends BaseCollectionHttpClient
{
    protected $authorizationData;

    public function __construct(array $params = [])
    {
        $this->settings = App::getSettings();
        $this->mainUrl = $this->settings['ua_settings']['ua_credit_info']['bki_hub_main_url'];
        parent::__construct($params);
    }

    public function getServiceName(): string
    {
        return 'hub_search';
    }

    public function getAuthorizationData(): array
    {
        return $this->authorizationData;
    }

    public function prepareAuthorizationData(): void
    {
        $this->authorizationUrl = $this->settings['ua_settings']['ua_credit_info']['bki_hub_auth_uri'];
        $this->requestMethod = 'post';
        $this->requestParams = [
            'username' => $this->settings['ua_settings']['ua_credit_info']['bki_hub_company_username'],
            'password' => $this->settings['ua_settings']['ua_credit_info']['bki_hub_company_password'],
            'grant_type' => 'password'
        ];
        $this->requestAuth = [
            $this->settings['ua_settings']['ua_credit_info']['bki_hub_auth_username'],
            $this->settings['ua_settings']['ua_credit_info']['bki_hub_auth_password']
        ];

        $this->getServiceId();
    }

    public function afterAuthorizationProcess(): void
    {
        $this->authorizationData = $this->getRequestData();
        $this->unsetRequestData();
    }

    /**
     * @return string
     */
    public function getServiceId(): string
    {
        if ($this->serviceId === null) {
            $serviceRepository = App::getRepository(ServiceSettings::class);
            $this->serviceId = $serviceRepository->findOneBy(['shortName' => $this->getServiceName()])->getId();
        }
        return $this->serviceId;
    }

    /**
     * @param string|null $date
     * @return string
     */
    protected function changeDate(?string $date): string
    {
        return $date !== null ? date('Y-m-d', strtotime($date)) : '';
    }

    /**
     * @param array $userInfo
     * @return array
     */
    protected function convertData(array $userInfo): array
    {
        foreach ($userInfo as $key => $item) {
            if (is_string($item)) {
                $userInfo[$key] = trim($item);
            }
        }

        $userInfo['birthday'] = $this->changeDate($userInfo['birthday']);
        $userInfo['passport_date'] = $this->changeDate($userInfo['passport_date']);
        $userInfo['date_get_okpo'] = $this->changeDate($userInfo['date_get_okpo']);

        foreach ($userInfo['address'] as $key => $address) {
            $userInfo['address'][$key]['address_date'] = $this->changeDate($address['address_date']);
        }

        foreach ($userInfo['connected_persons'] as $key => $person) {
            $userInfo['connected_persons'][$key]['birthday'] = $this->changeDate($person['birthday']);
            $userInfo['connected_persons'][$key]['agis_inn'] = $this->getAgisData($person['okpo']);
        }

        foreach ($userInfo['transports'] as $key => $person) {
            $userInfo['transports'][$key]['d_reg'] = $this->changeDate($person['d_reg']);
        }

        return $userInfo;
    }

    /**
     * @param string $inn
     * @return int|null
     */
    private function getAgisData(string $inn): ?int
    {
        $data = App::getRepository(UserUaCreditplus::class)
            ->findOneBy(['inn' => $inn]);
        if ($data !== null) {
            return $data->getUser()->getId();
        }
        return null;
    }
}
