<?php

namespace PayDay\Services\CollectionHttpClient;

use Core\App;

class HubPhoneCollectionHttpClient extends HubCollectionHttpClient
{
    /**
     * @param string $searchValue
     * @return array
     * @throws \ErrorException
     */
    public function search(string $searchValue): array
    {
        $value = $this->prepareSearchValue($searchValue);

        $this->requestHeaders = [
            'Authorization' => sprintf('Bearer %s', $this->jwt->access_token)
        ];
        $this->requestQuery = [
            $this->settings['ua_settings']['ua_credit_info']['bki_hub_search_phone_query'] => $value
        ];
        $this->makeRequest($this->settings['ua_settings']['ua_credit_info']['bki_hub_search_phone_uri']);
        $userInfo = json_decode($this->responseContent, true);

        if ($this->checkIssetContacts($userInfo)) {
            return $this->convertData($userInfo);
        }

        throw new \ErrorException('User is not found');
    }

    /**
     * @param array $userInfo
     * @return bool
     */
    private function checkIssetContacts(array $userInfo): bool
    {
        return isset($userInfo['contacts']) && $userInfo['contacts'] !== null;
    }

    /**
     * @param string $searchValue
     * @return string
     */
    private function prepareSearchValue(string $searchValue): string
    {
        if (!$searchValue) {
            return '';
        }
        if (mb_substr($searchValue, 0, 1) === '0') {
            $searchValue = '38' . $searchValue;
        }
        return ltrim($searchValue, '+');
    }
}
